import {Component, ComponentFactoryResolver, ComponentRef, Injector, ViewChild, ViewContainerRef} from '@angular/core';
import {DynamicComponent} from './components/dynamic/dynamic.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title = 'DynamicAngularExample';

  @ViewChild('dynamicContainer', {read: ViewContainerRef})
  dynamicContainer: ViewContainerRef;

  private _dynamicInstanceRefs: ComponentRef<DynamicComponent>[] = [];

  constructor(private componentFactoryResolver: ComponentFactoryResolver,
              private injector: Injector) { }

  async loadComponentAsync() {
    const {DynamicComponent} = await import('./components/dynamic/dynamic.component');
    const dynamicComponentFactory = this.componentFactoryResolver.resolveComponentFactory(DynamicComponent);
    this._dynamicInstanceRefs.push(
      this.dynamicContainer.createComponent(dynamicComponentFactory, null, this.injector));
  }

  destroyAllComponents() {
    this._dynamicInstanceRefs.forEach(component => {
      component.destroy();
    })
  }
}
